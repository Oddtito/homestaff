<?php

// Enqueue JS scripts and CSS styles for the theme
function homestaff_styles_scripts() {
	
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css'); 
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css' );
    wp_enqueue_style( 'theme-style', get_template_directory_uri() . '/assets/css/main.css');
    wp_enqueue_style( 'form-style', get_template_directory_uri() . '/assets/css/form.css');
    wp_enqueue_style( 'slick-slide-css', get_template_directory_uri() . '/assets/css/slick.min.css');

    // Enqueue JS scripts globally
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.6.2-respond-1.1.0.min.js', array( 'jquery' ), false, true );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), true, true );
	    wp_enqueue_script( 'slick-slide', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), true, true );
	    //wp_enqueue_script( 'respond', get_template_directory_uri() . '/assets/js/respond.js', array( 'jquery' ), true, true );
	    wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/js/skrollr.min.js', array( 'jquery' ), true, true );
		wp_enqueue_script( 'homestaff', get_template_directory_uri() . '/assets/js/homestaff.js', array( 'jquery' ), true, true );
}
add_action( 'wp_enqueue_scripts', 'homestaff_styles_scripts' );

// If WP Admin-bar is shown, then apply CSS to avoid conflict with fixed-top-navbar
function wpbs_adminbar_css() {
    if ( is_admin_bar_showing() ) { 
       echo '<style  type="text/css">
				@media (max-width: 767px) {
					.mm-fixed-top { top: 46px !important; }
					html.mm-opened body { margin-top: -46px; }
					#wpadminbar { position: fixed; }
				}
				@media (min-width: 768px) {
					.navbar-fixed-top {  margin-top:32px;}
					html.mm-opened body { margin-top: -32px; }
				}
			</style>';
    } 
}
add_filter( 'wp_head', 'wpbs_adminbar_css' );


function wpbs_setup() {
    // Switch default core markup for search form, comment form, and comments to output valid HTML5. 
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
	
    // Enable support for Post Thumbnails
    add_theme_support( 'post-thumbnails' );
	
    // Add RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );
}
add_action( 'after_setup_theme', 'wpbs_setup' );

// Register bootstrap navigation walker
require get_template_directory() . '/includes/wp_bootstrap_navwalker.php';

// Register theme components - Menus, Widgets and Sidebars
require get_template_directory() . '/includes/core-components.php';

// Template tags for Post Thumbnails
require get_template_directory() . '/includes/post-thumbnails.php';

// Template tags for Pagination & Post Navigation.
require get_template_directory() . '/includes/pagination.php';

// Template tags for Comments
require get_template_directory() . '/includes/comments-list.php';

// Template tags for excerpts (search results, blog list etc.)
//require get_template_directory() . '/includes/excerpts.php';



function hs_custom_excerpt_length( $length ) {
   return 20;
}
add_filter( 'excerpt_length', 'hs_custom_excerpt_length', 999 );



function hs_custom_excerpt_more($more) {
   global $post;
   return '...';
}
add_filter('excerpt_more', 'hs_custom_excerpt_more');



/**
* Is SubPage?
*
* Checks if the current page is a sub-page and returns true or false.
*
* @param  $page mixed optional ( post_name or ID ) to check against.
* @return boolean
*/
function ws_is_subpage( $page = null )
{
    global $post;
    // is this even a page?
    if ( ! is_page() )
        return false;
    // does it have a parent?
    if ( ! isset( $post->post_parent ) OR $post->post_parent <= 0 )
        return false;
    // is there something to check against?
    if ( ! isset( $page ) ) {
        // yup this is a sub-page
        return true;
    } else {
        // if $page is an integer then its a simple check
        if ( is_int( $page ) ) {
            // check
            if ( $post->post_parent == $page )
                return true;
        } else if ( is_string( $page ) ) {
            // get ancestors
            $parent = get_ancestors( $post->ID, 'page' );
            // does it have ancestors?
            if ( empty( $parent ) )
                return false;
            // get the first ancestor
            $parent = get_post( $parent[0] );
            // compare the post_name
            if ( $parent->post_name == $page )
                return true;
        }
        return false;
    }
};


###########################################################################
# SHORTCODE & CRUD VIEW ### alambertini@websolute.it ######################
###########################################################################
function getFieldName($id){
	global $wpdb;
	$nomecampo = $wpdb->get_row("select name from wp_frm_fields where id = '".$id."'");
	return $nomecampo->name;
}

function getFieldValue($utente,$item_id){
	global $wpdb, $utenti;
	$campi = $wpdb->get_results( 
	"select id, field_id, meta_value from wp_frm_item_metas where item_id = '".$item_id."'"
	);
	//[1678] => 378|194|Se sei straniero da che anno vivi in Italia?|2002
	foreach($campi as $campo){
		$utenti[$utente][$campo->id] = $item_id.'|'.$campo->field_id.'|'.getFieldName($campo->field_id).'|'.$campo->meta_value;
	}
}

function getSubGroups($chiave, $valore){

	global $wpdb;
	
	$valore = substr($valore, 0,6);
	
	$gruppi = $wpdb->get_results( 
		"select * from wp_frm_items where id <> '".$chiave."' and item_key like '%".$valore."%' order by id asc"
	);	
	$retArr = array();
	foreach($gruppi as $gruppo){
		$retArr[$gruppo->id] = $gruppo->item_key;
	}

	return $retArr;
}

function renderDomestici($atts){
	
	global $wpdb, $utenti;
	$utenti = array();
	$output = '';
    extract(shortcode_atts(array('id' => ''), $atts));
	
	/*array campi*/
	$labels = array(
		'nome'=>179,
		'cognome'=>180,
		'email'=>221,		
		'telefono'=>227,
		'iscrizione'=>'data_creazione',
		'foto'=>261,		
		'curriculum'=>262,
		'valutazione'=>384,
		'disponibilita'=>385,												
		'conoscenza'=>390,
		'mansione'=>256 //				 
	);


	$domestici = $wpdb->get_results( 
		"select * from wp_frm_items where form_id = '".$id."'order by id desc"
	);
	
	$dms_keys = array();
	foreach ( $domestici as $domestico ) 
	{
		$dms_keys['chiavi'][$domestico->id] =$domestico->item_key;
		$utenti[$domestico->id]['data_creazione'] = $domestico->created_at;
		$utenti[$domestico->id]['data_modifica'] = $domestico->updated_at;		
	}
	
	$dms = array();
	foreach($dms_keys['chiavi'] as $k => $v){
		$components = getSubGroups($k,$v);
		$dms['chiavi'][$k]['gruppi'] = $components;
			foreach($dms['chiavi'][$k]['gruppi'] as $kk => $vv){
				getFieldValue($k,$kk);
			}
	}
	
	//walker attraverso array UTENTI e recupero info
	
	
	echo '<pre>'; print_r($utenti);
}




function register_shortcodes(){
   add_shortcode('crud', 'renderDomestici');
}
add_action( 'init', 'register_shortcodes');

###########################################################################
# SHORTCODE & CRUD VIEW ### alambertini@websolute.it ######################
###########################################################################





function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'yes':'no';
    die();
}
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');
