<?php
/*
Template Name: Sitemap Page
*/
?>

<?php get_header();
class Thumbnail_walker extends Walker_page {
        function start_el(&$output, $page, $depth, $args, $current_page) {
        if ( $depth )
            $indent = str_repeat("\t", $depth);
        else
            $indent = '';
 
        extract($args, EXTR_SKIP);
        $css_class = array('page_item', 'page-item-'.$page->ID);
        if ( !empty($current_page) ) {
            $_current_page = get_page( $current_page );
            _get_post_ancestors($_current_page);
            if ( isset($_current_page->ancestors) && in_array($page->ID, (array) $_current_page->ancestors) )
                $css_class[] = 'current_page_ancestor';
            if ( $page->ID == $current_page )
                $css_class[] = 'current_page_item';
            elseif ( $_current_page && $page->ID == $_current_page->post_parent )
                $css_class[] = 'current_page_parent';
        } elseif ( $page->ID == get_option('page_for_posts') ) {
            $css_class[] = 'current_page_parent';
        }
 
        $css_class = implode( ' ', apply_filters( 'page_css_class', $css_class, $page, $depth, $args, $current_page ) );
        $txtvalue = get_field("breadcrumbs_label", $page->ID);
        if($txtvalue=="tito") {
				$output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . $txtvalue .'</a>';
					}
		else { $output .= $indent . '<li class="' . $css_class . '"><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) .'</a>'; }
 
        if ( !empty($show_date) ) {
            if ( 'modified' == $show_date )
                $time = $page->post_modified;
            else
                $time = $page->post_date;
 
            $output .= " " . mysql2date($date_format, $time);
        }
    }
}
	 ?>

<h2 id="pages">Mappa del sito</h2>
<ul>
<?php
// Add pages you'd like to exclude in the exclude here
$thumb_menu = new Thumbnail_walker();
wp_list_pages(
  array(
    'exclude' => '',
    'title_li' => '',
    'walker' => $thumb_menu
  )
);
?>
</ul>
<?php
foreach( get_post_types( array('public' => true) ) as $post_type ) {
  if ( in_array( $post_type, array('post','page','attachment') ) )
    continue;

  $pt = get_post_type_object( $post_type );

  echo '<h2>'.$pt->labels->name.'</h2>';
  echo '<ul>';

  query_posts('post_type='.$post_type.'&posts_per_page=-1');
  while( have_posts() ) {
    the_post();
    echo '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
  }

  echo '</ul>';
}
?>
</ul>
</div>
 <?php get_footer(); ?>