
<?php get_header(); ?>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?> 
<section class="">
    <div class="row">
        <div class="col-md-6">


	<?php while ( have_posts() ) : the_post(); ?>
	  	   <p class="abstract">
	  	<?php the_content(); ?>
	  	   </p>
     
	<?php endwhile; ?>
		

  </div>
  <div class="text-center col-md-6">
			<div id="map-canvas" class="map"></div>
		</div>
   </div>
</section>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyClmQvBU5eqTS3k_BEToj9aWlCpBgrGhmw&sensor=false"></script>
<script>
    var gmap = document.getElementById('map-canvas');
    var map;
    var settings = {
    	home: { latitude: 45.4801348, longitude: 9.1697869 },
    	text: '<div><strong>MV Home Staff Consulting S.a.s. di Mariapaola Verga & C.</strong></div><p>Via Manfredini 1, 20154 Milano<br/>Fax: +(39) 0243917266<br/><a href="mailto:info@homestaff.it">info@homestaff.it</a></p><p>Partita Iva e Codice Fiscale: 06692870964<br/>Aut. Min. 39/0002047/MA004.A003</p>',
        //icon_url: '/img/marker.png',
        zoom: 16
    }

    var coords = new google.maps.LatLng(settings.home.latitude, settings.home.longitude);



    var options = {
        zoom: settings.zoom,
        scrollwheel: true,
        center: coords,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        scaleControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT
        },
        overviewMapControl: true
    };

    map = new google.maps.Map(gmap, options);

    var icon = {
        url: settings.icon_url,
        origin: new google.maps.Point(-5, -5)
    };

    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        //icon: icon,
        draggable: false
    });

    var info = new google.maps.InfoWindow({
        content: settings.text
    });

    google.maps.event.addListener(marker, 'click', function () {
        info.open(map, marker);
    });


    // 5. COLOR SETTINGS

    var styles = [
        /*{
        featureType: "all",
        stylers: [
            { saturation: -80 }
        ]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [
            { hue: "#b6ded1" },
            { saturation: 10 }
        ]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [
            { hue: "#b6ded1" },
            { saturation: 50 }
        ]
    }, {
        featureType: 'poi.school',
        elementType: 'geometry',
        stylers: [
            { hue: '#b6ded1' },
            { lightness: -5 },
            { saturation: 50 }
        ]
    }, {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [
            { hue: '#b6ded1' },
            { lightness: -10 },
            { saturation: 60 }
        ]
    }, {
        featureType: 'poi.park',
        elementType: 'labels.icon',
        stylers: [
            { hue: '#b6ded1' },
            { lightness: -15 },
            { saturation: 99 }
        ]
    }*/
    ];

    map.setOptions({ styles: styles });
</script>
</div>
<?php get_footer(); ?>