 <!-- /container -->
		<footer class="footer">
             <div class="row">
            <div class="container">
               
                    <div class="col-xs-10 text-left">
                     <?php wp_nav_menu(
                        array(
                            'menu'              => 'prefooter-menu',
                            'theme_location'    => 'prefooter-menu',
                            'depth'             => 2,
                            'container'         => false,
                            'menu_class'        => 'footer')
                        );
                    ?>

                    </div>
                    <div class="col-xs-2 social text-right">
                        <!-- a href="#" rel="me"><i class="fa fa-facebook"></i></a -->
                        <a href="https://twitter.com/Domestici" rel="me"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </footer>
         <div class="footer gray">
          
                <div class="row">
            <div class="container">
                    <div class="col-sm-10">
                        <ul>
                            <li>&copy; 2015 MV HOME STAFF CONSULTING SAS di Mariapaola Verga &amp; C.</li>
                            <li>P.IVA/CF 06692870964</li>
                            <li>Aut. Min. 39/0002047/MA004.A003</li>
                        </ul>
                        <ul>
                         <?php wp_nav_menu(
                        array(
                            'menu'              => 'footer-menu',
                            'theme_location'    => 'footer-menu',
                            'depth'             => 2,
                            'container'         => false,
                            'menu_class'        => '')
                        );
                    ?>

                        </ul>
                    </div>
                    <div class="col-sm-2 text-right">
                        <a title="Digital Marketing" target="_blank" href="http://www.websolute.com">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/DigitalMarketing.png' ?>" alt="Digital Marketing">
                            Websolute
                        </a>

                    </div>
                </div>
            </div>
        </div>
        </div>
		<?php wp_footer(); ?>
	</div> <!-- /wrapper -->
	
	</body>
</html>