<?php
$type = 'bigcta';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  echo '<ul id="arealist">';	
  while ($my_query->have_posts()) : $my_query->the_post(); ?>

            <li style="background-image:url(<?php the_field('cta_img'); ?>)"><p><?php the_title(); ?></p><br /><a class="cta" href="<?php the_field('cta_link'); ?>" title="<?php the_title(); ?>">Entra</a></li>
        
		<?php
	    		endwhile;

	echo '</ul>';
	wp_reset_query();
	};

		?>
<div class="clearfix"></div>