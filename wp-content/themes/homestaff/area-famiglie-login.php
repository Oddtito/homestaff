
<?php
/*
Template Name: Area Famiglie - Login
*/
?>


<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
 <section class="no-pad">
    <div class="row">
        <div class="col-md-12 wow">
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?> 
<?php echo '<!-- ' . get_page_template() . ' -->'; ?>
        </div>
  </div>
    </section>



</div>
            <section class="inner-xs no-pad">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 wow ">
                            <div class="html">
                                <div>
                                <?php the_content(); ?>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>	
	 

<?php endwhile; ?> 
<?php get_footer(); ?>
