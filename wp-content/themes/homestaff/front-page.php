<?php
/*
Template Name: Full Width
*/
?>

<?php get_header(); ?>

<?php if( function_exists( 'wp_slick_slider' ) ){ wp_slick_slider( 'home' ); } ?>

<section class="container">
			<div class="row">
				<div class="text-center col-md-8 col-md-offset-2">
					
<?php wp_reset_query(); 
	
	if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php the_content() ?>

	<?php endwhile; else: ?>

		<p><?php _e('Sorry, this page does not exist.'); ?></p>

	<?php endif; ?>

				</div>
			</div>
</section>
   <?php get_template_part( 'partials/section', 'bigcta' ); ?>

   <?php get_template_part( 'partials/section', 'familyarea' ); ?>

   <?php get_template_part( 'partials/section', 'testimonials' ); ?>
	
   <div class="parallax" style="background:url(<?php the_field("parallax"); ?>)" data-bottom-top="background-position:50% 100%" data-top-bottom="background-position:50% 0%"></div>
  
   <?php get_template_part( 'partials/section', 'servantarea' ); ?>
</div>

<?php get_footer(); ?>