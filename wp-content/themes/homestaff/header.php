<?php global $current_user; ?>
<!DOCTYPE html>
<html <?php  language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/images/favicon.png' ?>">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri() . '/assets/images/apple-touch-icon.png' ?>">
        <title><?php wp_title( '|' , 1, 'right' ); ?> <?php bloginfo( 'name' ); ?></title>
        <link href='http://fonts.googleapis.com/css?family=Roboto:100italic,300italic,400italic,600italic,700italic,700,100,300,600,400' rel='stylesheet' type='text/css'>
        <?php wp_head(); ?>
    
    </head>
    <body <?php body_class( $class ); ?> style="overflow: scroll;">
	    
    <div class="wrapper">
	<nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php bloginfo('siteurl'); ?>">
						<img alt="MV Home Staff babysitter e badanti milano" src="<?php echo get_template_directory_uri() . '/assets/images/logo.png' ?>">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="bs-nav">
                   	<ul class="nav navbar-nav pull-right">
                   <?php wp_nav_menu(
                        array(
                            'menu'              => 'primary-menu',
                            'theme_location'    => 'primary-menu',
                            'depth'             => 2,
                            'container'         => false,
                            //'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            // 'walker'            => new wp_bootstrap_navwalker(),
                            'menu_class'        => 'nav navbar-nav pull-right')
                        );
                    ?>
                   	</ul>
                </div>
            </div>
  </nav>
   
<?php
	if ( is_singular('sdi_course') ) {
?> 
<section class="banner" style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/bg-banner3.jpg' ?>);">
<h1>Corsi della Scuola Italiana Domestici</h1>
</section>
  
<?php }
if (( is_singular('inserzione')) || ( is_page_template( 'archive-inserzione.php' ))) {
?> 
  <section class="banner" style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/bg-banner2.jpg' ?>); color:#fff">
<h1>inserzioni</h1>
</section>
  
<?php }
if ( is_singular('testimonial') ) {
?> 
  <section class="banner">
<h1>Testimonial</h1>
</section>
  
<?php }
if (is_page() && !is_front_page()) {

if ((is_page_template( 'area-domestici.php' )) || (ws_is_subpage(118))){
?> 
    <section class="banner" style="background-image:url(<?php echo get_template_directory_uri() . '/assets/images/bg-banner2.jpg' ?>); color:#fff">
            <h1><?=the_title(); ?></h1>
        </section>
  
<?php } else if (!( is_page_template( 'archive-inserzione.php' ))) {?>  

<section class="banner">
<h1><?=the_title(); ?></h1>
</section>
  
<?php } 
	} 
?>


  <div class="container" id="page-content">