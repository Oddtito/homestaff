<?php $current_id = get_the_id();?>
<section class="bg-red text-center">
			<div class="container">
				<h2>AREA DOMESTICO</h2>
				<p class="abstract_big">Cerchi lavoro?</p>
				<div class="row boxes w75p">
					<div class="col-sm-4">
						<div class="box">
							<h3>Area domestici</h3>
							<ul>
									<?php wp_nav_menu(
			                        array(
			                            'menu'              => 'servantarea-menu-pages',
			                            'theme_location'    => 'servantarea-menu-pages',
			                            'depth'             => 2,
			                            'container'         => false,
			                            'menu_class'        => '')
			                        );
			                    	?>
							</ul>
							
							<?php 
								$page_servant = get_pages(array(
								'meta_key' => '_wp_page_template',
								'meta_value' => 'area-domestici.php'
								));
							?>
							<a href="<?php echo get_permalink($page_servant->ID);?>">
								
								
								<strong>vai a area domestici</strong></a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box">
							<h3>Inserzioni</h3>
							<ul>
									<?php
									$type = 'inserzione';
									$args=array(
										'post_type' => $type,
										'post_status' => 'publish',
										'posts_per_page' => -1,
										'caller_get_posts'=> 1
									);
									
									$my_query = null;
									$my_query = new WP_Query($args);
									if( $my_query->have_posts() ) {
										while ($my_query->have_posts()) : $my_query->the_post(); ?>
									
										<li><a href="<?php the_permalink() ?>"><?php the_title();?></a></li>
									
									    <?php
										endwhile;
									}
									wp_reset_query();  // Restore global post data stomped by the_post().
									?>
							</ul>
						
							<a href="<?php bloginfo('siteurl'); ?>/inserzioni/"><strong>Vedi tutte le inserzioni</strong></a>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="box">
							<h3>Corsi</h3>
							<ul>
									<?php
									$type = 'sdi_course';
									$args=array(
										'post_type' => $type,
										'post_status' => 'publish',
										'posts_per_page' => 1,
										'caller_get_posts'=> 1
									);
									
									$my_query = null;
									$my_query = new WP_Query($args);
									$last_permalink=get_the_permalink();
									if( $my_query->have_posts() ) {
										while ($my_query->have_posts()) : $my_query->the_post(); 
										$last_permalink=get_the_permalink()
										?>
									
										<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
									
									    <?php
										endwhile;
									}
									wp_reset_query();  // Restore global post data stomped by the_post().
									?>
							</ul>
							
							
							<a href="<?=$last_permalink;?>"><strong>Vedi tutti</strong></a>
						</div>
					</div>
				</div>

<?php
if ( is_home() ) {
    // This is the blog posts index
    ?>	<a class="cta" href="#">Entra in area domestico</a><?php
} else {
    // This is not the blog posts index
        ?><?php
}
?>


			
			</div>
		</section>  	
