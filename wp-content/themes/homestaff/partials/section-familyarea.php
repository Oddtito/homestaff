<section class="bg-gray text-center areas">
			<div class="container">
				<h2>AREA FAMIGLIE</h2>
				<!-- p class="abstract_big">AREA FAMIGLIE</!--p -->
				<div class="row boxes">
						<div class="col-md-3 col-xs-6">
							<div class="box">
								<h3>Ricerca e selezione</h3>
								<ul>
									<?php wp_nav_menu(
			                        array(
			                            'menu'              => 'familyarea-menu-up',
			                            'theme_location'    => 'familyarea-menu-up',
			                            'depth'             => 2,
			                            'container'         => false,
			                            'menu_class'        => '')
			                        );
			                    ?>
								</ul>
									<strong>&nbsp;</strong>
									<div class="city_list">
										<strong>Ricerca anche a</strong>
								<?php echo strip_tags (wp_nav_menu(
			                        array(
			                            'menu'              => 'familyarea-menu-down',
			                            'theme_location'    => 'familyarea-menu-down',
			                            'container'         => false,
			                            'menu_class'        => '',
			                            'after'				=> '<span>,</span>',
			                            'echo'              => false,
										'items_wrap'        => '%3$s',
										'depth'             => 1
			                            )
			                            
			                        ), '<a> <span>,</span>' );
			                    ?>
									</div>
							</div>
						</div>
						<div class="col-md-3 col-xs-6">
							<div class="box">
								<h3>Servizi</h3>
								<ul>
									<?php wp_nav_menu(
			                        array(
			                            'menu'              => 'familyarea-menu-courses',
			                            'theme_location'    => 'familyarea-menu-courses',
			                            'depth'             => 2,
			                            'container'         => false,
			                            'menu_class'        => '')
			                        );
			                    ?>
								</ul>
							</div>
						</div>
					<div class="fam-box-cont col-xs-12 col-md-6 ">
						<div class="bg-red2 text-center fam-box">
							<img src="<?php echo get_template_directory_uri()?>/assets/images/family_search_icon.png" />
							<h2>Risparmia</h2>
							<p class="abstract">Scegli da te il tuo domestico</p>
							<div class="row">
								<div class="col-xs-6 text-right">
									<a class="cta" href="/areafamiglie/ricerca-e-selezione/fai-da-te/" title="AREA FAMIGLIE">Fai da te</a>
								</div>
								<div class="col-xs-6">
									<a class="cta" href="<?php echo wp_login_url(); ?>" title="Fai da te">Entra</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<a class="cta" href="http://homestaff-wp.wslabs.it/areafamiglie/registrazione/" title="Registrazione">RICERCA PERSONALE</a>
			</div>
		</section>
