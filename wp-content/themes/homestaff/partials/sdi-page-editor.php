<?php
// check if the flexible content field has rows of data
if( get_field('sdi-page-editor') ):

	// loop through the rows of data
	while ( has_sub_field('sdi-page-editor') ) :


		
	if( get_row_layout() == 'three_button' ):

?> 	<section class="inner-sm">
                <div class="col-md-4 wow ">
	        			<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("1_three_button_link")?>" target="_self"><?php the_sub_field("1_three_button_label")?></a></div>
			    </div>
                <div class="col-md-4 wow ">
	        			<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("2_three_button_link")?>" target="_self"><?php the_sub_field("2_three_button_label")?></a></div>
			    </div>
                <div class="col-md-4 wow ">
	       			<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("3_three_button_link")?>" target="_self"><?php the_sub_field("3_three_button_label")?></a></div>
			     </div>
           </section>        
                    
<?php elseif( get_row_layout() == 'accordion' ):?>
    <section class="no-pad  ">
                                                         <div class="col-md-12 wow ">
        <div class="panel-group" id="accordion_1-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
	                    <?php $accordion_id = preg_replace('/[^A-Za-z0-9]/', '', get_sub_field("accordion_title")); ?>
                        <a data-toggle="collapse" data-parent=" <?php the_sub_field("accordion_id")?>" href="#<?=$accordion_id ?>">
 <?php the_sub_field("accordion_title")?>
                        </a>
                    </h4>
                </div>
                <div id="<?=$accordion_id ?>" class="panel-collapse collapse">
                    <div class="panel-body">

        <div class="html">
            <span> <?php the_sub_field("accordion_body")?>.</span>
        </div>

                    </div>
                </div>
        
        </div>
                                    </div>
                            </div>
                    </section>
<?php
			elseif( get_row_layout() == 'two_columns' ):

?>
      <section class=" ">           
	             <div class="col-md-6 wow ">
                 <?php the_sub_field("two_columns_sx")?>
                </div>

                <div class="col-md-6 wow ">
                 <?php the_sub_field("two_columns_dx")?>
                </div>
    </section>
        <?php
		elseif( get_row_layout() == 'dx_img' ):?>
         <section class="">
	               <div class="row">
                               <div class="col-md-6 wow">
                                            <?php the_sub_field("dx_img_txt")?>
                                </div>
                                <div class="col-md-6 wow image"><?php if (get_sub_field("dx_img_img")!=""):?><img src="<?php the_sub_field("dx_img_img")?>" class="img-circle"><?php endif;?></div>
                    </div>
                                                        
         </section>

        <?php

		elseif( get_row_layout() == 'sx_img' ):
		        ?>
		               <section class="">
	               <div class="row">
		               
                               <div class="col-md-6 wow image"><?php if (get_sub_field("sx_img_img")!=""):?><img src="<?php the_sub_field("sx_img_img")?>" class="img-circle"><?php endif;?></div>
                               <div class="col-md-6 wow">
                                            <?php the_sub_field("sx_img_txt")?>
                               </div>
                    </div>
                                                        
         </section>
        <?php


								endif;

							endwhile;

						else :

							// no layouts found

							endif;

?>
