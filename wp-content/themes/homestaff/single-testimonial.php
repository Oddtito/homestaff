<?php get_header(); ?>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?> 
<section class="annunci">
<div class="row">
<div class="col-sm-3">
<ul>

<?php
$type = 'testimonial';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
 			
	<li><a href="<?php the_permalink() ?>"><?php the_title();?></a></li>
						
    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>

</ul>
</div>

  <div class="col-sm-8 col-sm-offset-1">

	<?php while ( have_posts() ) : the_post(); ?>
       
        <h1><?php the_title(); ?></h1>
<div class="LayoutBuilder">        
	<section class=" ">
<div class="fluid">
<div class="row">
<div class="col-md-3 wow ">
	<div class="image ">
		<?php $url = wp_get_attachment_image_src(get_post_thumbnail_id() , 'thumbnail' );?>	
		<img class="img-circle img-responsive" src="<?=$url[0]; ?>">
	</div>
</div>
        
        <div class="col-md-9 wow ">
         <div class="html"> <?php the_content(); ?></div>
        </div>
</div>
</section>

      <?php endwhile;  ?>
		
  </div>
  
      </div>
</div>
</section>
</div>
<?php get_footer(); ?>