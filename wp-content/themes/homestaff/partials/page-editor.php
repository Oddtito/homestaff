<?php

// check if the flexible content field has rows of data
if( get_field('page-editor') ):

	// loop through the rows of data
	while ( has_sub_field('page-editor') ) :


		
	if( get_row_layout() == 'three_button' ):

?> 	<section class="inner-sm  ">
       <div class="container">
             <div class="row">
                <div class="col-md-4 wow ">
	                <div class="row">
						<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("1_three_button_link")?>" target="_self"><?php the_sub_field("1_three_button_label")?></a></div>
					</div>
                </div>
                <div class="col-md-4 wow ">
	                <div class="row">
						<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("2_three_button_link")?>" target="_self"><?php the_sub_field("2_three_button_label")?></a></div>
					</div>
                </div>
                <div class="col-md-4 wow ">
	                <div class="row">
						<div class="text-center col-md-8 col-md-offset-2"><a class="cta" href="<?php the_sub_field("3_three_button_link")?>" target="_self"><?php the_sub_field("3_three_button_label")?></a></div>
					</div>
                </div>
            </div>
        </div>
    </section>        
                    
<?php elseif( get_row_layout() == 'accordion' ):?>
    <section class="no-pad  ">
                        <div class="container">
                            <div class="row">
                                    <div class="col-md-12 wow ">
        <div class="panel-group" id="accordion_1-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
	                    <?php $accordion_id = preg_replace('/[^A-Za-z0-9]/', '', get_sub_field("accordion_title")); ?>
                        <a data-toggle="collapse" data-parent=" <?php the_sub_field("accordion_id")?>" href="#<?=$accordion_id ?>">
 <?php the_sub_field("accordion_title")?>
                        </a>
                    </h4>
                </div>
                <div id="<?=$accordion_id ?>" class="panel-collapse collapse">
                    <div class="panel-body">

        <div class="html">
            <span> <?php the_sub_field("accordion_body")?>.</span>
        </div>

                    </div>
                </div>
            </div>

        </div>
                                    </div>
                            </div>
                        </div>
                    </section>
<?php
			elseif( get_row_layout() == 'two_columns' ):

?>
      <section class=" ">
        <div class="container">
            <div class="row">
                <div class="col-md-7 wow ">
                 <?php the_sub_field("two_columns_sx")?>
                </div>

                <div class="col-md-5 wow ">
                 <?php the_sub_field("two_columns_dx")?>
                </div>
            </div>
        </div>
    </section>
        <?php

			elseif( get_row_layout() == 'parallax_img' ):?>
			  <section class="lb_parallax medium classic" data-bottom-top="background-position:50% 0%" data-top-bottom="background-position:50% 100%" style="background:url(<?php the_sub_field("parallax_img_url")?>) no-repeat 50% 0;">
			            <div class="container inner-md">
			                <div class="row">
			                    <div class="col-md-8 col-sm-10 center-block text-center">
			                        <header class="dark">
			                                           <div class="text"><p>&nbsp;</p>
			
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														
														<p>&nbsp;</p>
														</div>
			                        </header>
			                    </div>
			                </div>
			            </div>
			        </section>
			<?php
			elseif( get_row_layout() == 'sx_bg' ):?>
          <section class="no-pad  ">
                <div class="fluid">
                    <div class="row">
                        <div class="col-md-12 wow ">
                            <section class="bg bg-left" style="background-image:url(<?php the_sub_field("sx_bg_img")?>); background-position: 18% center;">
                                <div class="container">
                                    <div class="row">
                                         <div class="col-md-4 col-md-offset-6">
                                            <?php the_sub_field("sx_bg_txt")?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        <?php

		elseif( get_row_layout() == 'dx_bg' ):?>
          <section class="no-pad  ">
                <div class="fluid">
                    <div class="row">
                        <div class="col-md-12 wow ">
                            <section class="bg bg-right" style="background-image:url(<?php the_sub_field("dx_bg_img")?>); background-position: center center;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-2">
                                            <?php the_sub_field("dx_bg_txt")?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>
        <?php

		elseif( get_row_layout() == 'dx_img' ):?>
         <section class="no-pad  ">
                <div class="fluid">
                    <div class="row">
                        <div class="col-md-12 wow ">
                            <section class="img bg-col-light-gray">
                                <div class="container">
                                    <div class="row">
                                            <div class="col-md-5 col-md-offset-2 col-xs-8">
                                            <?php the_sub_field("dx_img_txt")?>
                                       </div>
                                        <div class="col-md-3 col-xs-4"><?php if (get_sub_field("dx_img_img")!=""):?><img src="<?php the_sub_field("dx_img_img")?>"><?php endif;?></div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </section>

        <?php

		elseif( get_row_layout() == 'simple_txt' ):?>
        <section class="no-pad" style="margin-bottom: 50px;">
                <div class="fluid">
                    <div class="row">
	                     <div class="container">
                        <div class="col-md-12 wow" style="padding: 0;">
                           
                                  <?php the_sub_field("simple_txt_corpo")?>
                        
                        </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php
	        
			
		elseif( get_row_layout() == 'testimonials_section' ): if (get_sub_field("ok_testimonials")) : get_template_part( 'partials/section', 'testimonials' ); endif;
			       
		elseif( get_row_layout() == 'family_section' ): if (get_sub_field("ok_family")) : get_template_part( 'partials/section', 'familyarea' );  endif;
		
		elseif( get_row_layout() == 'servant_section' ): if (get_sub_field("ok_servant")) : get_template_part( 'partials/section', 'servantarea' ); endif;	       	

		elseif( get_row_layout() == 'bigcta_section' ): if (get_sub_field("ok_big_cta")): get_template_part( 'partials/section', 'bigcta' ); endif;
		
		elseif( get_row_layout() == 'sx_img' ):
		        ?>
		        <section class="no-pad  ">
		                <div class="fluid">
		                    <div class="row">
		                        <div class="col-md-12 wow ">
		                            <section class="img bg-col-light-gray">
		                                <div class="container">
		                                    <div class="row">
		                                        <div class="col-md-3 col-md-offset-2 col-xs-4"><?php if (get_sub_field("sx_img_img")!=""):?><img src="<?php the_sub_field("sx_img_img")?>"><?php endif;?></div>
		
		                                        <div class="col-md-5 col-xs-8">
		                                            <?php the_sub_field("sx_img_txt")?>
		                                       </div>
		                                    </div>
		                                </div>
		                            </section>
		                        </div>
		                    </div>
		                </div>
		            </section>

        <?php


								endif;

							endwhile;

						else :
?>                <section class="">
		                <div class="fluid">
		                    <div class="row">
			                    <div class="container">

						<?php 	the_content(); ?>
								</div>
		                    </div>
		                </div>
					</section> 
					<?php 
					endif;
					?>
