<?php $current_id = get_the_id(); get_header(); ?>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
<section class="corsi">
<div class="row">
<div class="col-sm-3">
<ul>

<?php
$type = 'sdi_course';
$args=array(
	'post_type' => $type,
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'caller_get_posts'=> 1
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
	while ($my_query->have_posts()) : $my_query->the_post(); ?>

	<li <?php if($current_id==get_the_id()){ echo "class='active'";} ?>><a href="<?php the_permalink() ?>"><?php the_title();?></a></li>

    <?php
	    	endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>

</ul>
</div>

  <div class="col-sm-8 col-sm-offset-1">

	<?php while ( have_posts() ) : the_post(); ?>

    <div class="row">
					<div class="col-xs-12">
						<img id="logo" src="<?php echo get_template_directory_uri() . '/images/logo_domestici.png' ?>" alt="logo" />
						<h1><?php the_title(); ?></h1>
					</div>
    </div>


	<div class="row">
		   <?php if (get_the_content()!=""){
		   		the_content();
		   				} else {
			   				get_template_part( 'partials/sdi', 'page-editor' ); }
							?>

  	</div>
      <?php endwhile;  ?>
	</div>
</section>

</div>
   <?php get_template_part( 'partials/section', 'servantarea' ); ?>
<?php get_footer(); ?>