	<section class="testimonial">
			<div class="container text-center">
				<h2>Testimonial</h2>
				<div class="row">

								<?php
$type = 'testimonial';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);

$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
 					<div class="col-md-5 col-md-offset-1">
							<div class="row">			
								<div class="col-xs-3">
									<?php $url = wp_get_attachment_image_src(get_post_thumbnail_id() , 'small' );?>	
									<img src="<?=$url[0]; ?>" />
								</div>
								<div class="col-xs-8 col-xs-offset-1">
									<p><?php echo substr(get_the_excerpt(), 0,53); ?>...</p>
									<div class="divider"></div>
									<p class="family"></p>
										<a href="<?php the_permalink() ?>" class="cta">Scopri di piu</a>
								</div>
							</div>
					</div>
						
    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
	
				</div>
			</div>
</section>
