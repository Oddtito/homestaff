<?php
/*
Plugin Name: Really Simple Breadcrumb
Plugin URI: http://www.christophweil.de
Description: This is a really simple WP Plugin which lets you use Breadcrumbs for Pages!
Version: 1.0.2
Author: Christoph Weil
Author URI: http://www.christophweil.de
Update Server: 
Min WP Version: 3.2.1
Max WP Version: 
*/


function simple_breadcrumb() {
    global $post;
	$separator = " <i class='fa fa-angle-right'></i> "; // Simply change the separator to what ever you need e.g. / or >
	
    echo '<div class="breadcrumb">';
	if (!is_front_page()) {
		echo '<a href="';
		echo get_option('home');
		echo '">';
		$frontpage_id = get_option('page_on_front');
		echo get_the_title($frontpage_id);
		echo "</a> ".$separator;
		if ( is_category() || is_single() ) {
			the_category('');
			if ('inserzione' == get_post_type()){   
				  		?>
				  		 <a href="<?php bloginfo('siteurl'); ?>/selezione_personale_domestico_area_domestici/">Area Domestici</a><?=$separator;?><a href="<?php bloginfo('siteurl'); ?>/inserzioni/">Inserzioni</a>
				  		<?php
				 
			} elseif ('testimonial' == get_post_type()){   
				
				$queryObject = new WP_Query( 'post_type=testimonial&posts_per_page=1' );
				if ($queryObject->have_posts()) {
				  while ($queryObject->have_posts()) {
				  		$queryObject->the_post();
				  		?>
				  		 <a href="<?php the_permalink(); ?>">Testimonials</a></li>
				  		<?php
				  		}
				 
				  
				  }  		wp_reset_query();
				 
			} elseif ('sdi_course' == get_post_type()){   
				
				$queryObject = new WP_Query( 'post_type=sdi_course&posts_per_page=1' );
				if ($queryObject->have_posts()) {
				  while ($queryObject->have_posts()) {
				  		$queryObject->the_post();
				  		?>
				  		 <a href="<?php bloginfo('siteurl'); ?>/selezione_personale_domestico_area_domestici/">Area Domestici</a><?=$separator;?><a href="<?php the_permalink(); ?>">Corsi</a>
				  		<?php
				  		}
				 
				  
				  }  		wp_reset_query();
				 
			} elseif ( is_single() ) {
				the_title();
			}
			
		} elseif ( is_page() && $post->post_parent ) {
			$home = get_page(get_option('page_on_front'));
			for ($i = count($post->ancestors)-1; $i >= 0; $i--) {
				if (($home->ID) != ($post->ancestors[$i])) {
					echo '<a href="';
					echo get_permalink($post->ancestors[$i]); 
					echo '">';
					$padre = get_field("breadcrumbs_label", $post->ancestors[$i]);
					if ($padre!="") {
				      echo $padre.$separator;
				   	} else { echo get_the_title($post->ancestors[$i]).$separator;}
					echo "</a>";
					}
					
			}
			if (get_field("breadcrumbs_label")!="") {
					echo the_field("breadcrumbs_label");
		      		} else { the_title();}
		} elseif (is_page()) {
		      if (get_field("breadcrumbs_label")!="") {
			     echo the_field("breadcrumbs_label");
		      } else { the_title();}
		} elseif (is_404()) {
			echo "404";
		}
	} else {
		bloginfo('name');
	}
	echo '</div>';
}
?>