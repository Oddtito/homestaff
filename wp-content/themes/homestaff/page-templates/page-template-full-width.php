
<?php get_header(); ?>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?> 
   <section class="no-pad">
    <div class="row">
        <div class="col-md-12 wow">


	<?php while ( have_posts() ) : the_post(); ?>
	 
	  	<?php the_content(); ?>
	 
	<?php endwhile; ?>
		

  </div>
    </section>
</div>

<?php get_footer(); ?>
