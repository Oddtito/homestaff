<?php if ( true ) : ?>
<div class="login-form-container">
	<?php if ( $attributes['show_title'] ) : ?>
		<h2><?php _e( 'Sign In', 'personalize-login' ); ?></h2>
	<?php endif; ?>
	<form method="post" action="<?php echo wp_login_url(); ?>">

							<div class="form-group">
								<label class="col-sm-2 control-label" for="user_login"><?php _e( 'Email', 'personalize-login' ); ?></label>
								<div class="col-sm-4">
									<input type="text"  class="form-control" name="log" id="user_login" placeholder="Email" />								</div>
								<label class="col-sm-2 control-label" for="loginPassword"><?php _e( 'Password', 'personalize-login' ); ?></label>
								<div class="col-sm-4">
									<input type="password" name="pwd" id="user_pass" placeholder="Password" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10"><br />
									<a href="<?php echo wp_lostpassword_url(); ?>" class="lost-password"><?php _e( 'Forgot your password?', 'personalize-login' ); ?></a>
								</div>
							</div>
							<div class="form-group text-center">
								<button type="submit" class="btn btn-default">Accedi</button>
							</div>
						
		</form>

	<!-- Show errors if there are any -->
	<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p class="login-error">
				<?php echo $error; ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>

	<!-- Show logged out message if user just logged out -->
	<?php if ( $attributes['logged_out'] ) : ?>
		<p class="login-info">
			<?php _e( 'You have signed out. Would you like to sign in again?', 'personalize-login' ); ?>
		</p>
	<?php endif; ?>

	<?php if ( $attributes['registered'] ) : ?>
		<p class="login-info">
			<?php
				printf(
					__( 'You have successfully registered to <strong>%s</strong>. We have emailed your password to the email address you entered.', 'personalize-login' ),
					get_bloginfo( 'name' )
				);
			?>
		</p>
	<?php endif; ?>

	<?php if ( $attributes['lost_password_sent'] ) : ?>
		<p class="login-info">
			<?php _e( 'Check your email for a link to reset your password.', 'personalize-login' ); ?>
		</p>
	<?php endif; ?>

	<?php if ( $attributes['password_updated'] ) : ?>
		<p class="login-info">
			<?php _e( 'Your password has been changed. You can sign in now.', 'personalize-login' ); ?>
		</p>
	<?php endif; ?>

	
</div>


<div class="col-sm-12 text-center">
						<p>Registrati adesso</p>
						<a href="http://www.homestaff.it/register" class="cta">Acquista il servizio</a>
					</div>
					
<?php else : ?>
	<div class="login-form-container">
			<div class="col-sm-12">
				</div>
<?php endif; ?>
