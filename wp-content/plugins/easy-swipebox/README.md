# Easy SwipeBox (Wordpress Plugin)
This Wordpress plugin enable [SwipeBox jQuery extension](http://brutaldesign.github.io/swipebox/ "SwipeBox jQuery extension") on all links to image or Video (Youtube / Vimeo).

## Plugin main features

> Enqueuing of SwipeBox Javascript and CSS files. Set WP_DEBUG to true for the uncompressed files.
> Autodetection of links to images or videos. You can exclude/include media types from the autodetection section here below.
