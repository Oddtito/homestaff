<?php
/*
Template Name: Form */
?>

<?php get_header(); ?>
<?php if (!get_field('escludi_breadcrumbs')): ?>
 <section class="no-pad">
    <div class="row">
        <div class="col-md-12 wow">
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
        </div>
  </div>
    </section>
    <?php endif; ?>
</div>

<?php while ( have_posts() ) : the_post(); ?>
<section class="no-pad">
<?php the_content(); ?><br><br>
</section>


<?php endwhile; ?>
<?php if (ws_is_subpage(118)) {
	get_template_part( 'partials/section', 'servantarea' ); 
}?>
<?php get_footer(); ?>
