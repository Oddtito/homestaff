
<?php
/*
Template Name: Area Famiglie - Fai da Te
*/
?>


<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
 <section class="no-pad">
    <div class="row">
        <div class="col-md-12 wow">
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?> 
        </div>
  </div>
    </section>


</div>
<?php get_template_part( 'partials/page', 'editor' ); ?>

            <section class="inner-xs no-pad">
                <div class="fluid">
                    <div class="row">
                        <div class="col-md-12 wow ">
                            <div class="html">
                                <div>
                                <?php the_content(); ?>
                                
                                <br /><br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>	
	 

<?php endwhile; ?> 
<?php get_footer(); ?>
