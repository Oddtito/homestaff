
<?php get_header(); ?>
<?php if (!get_field('escludi_breadcrumbs')): ?>
 <section class="no-pad">
    <div class="row">
        <div class="col-md-12 wow">
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
        </div>
  </div>
    </section>
    <?php endif; ?>
</div>

<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'partials/page', 'editor' ); ?>

<?php endwhile; ?>

<?php get_footer(); ?>
